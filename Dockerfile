FROM debian:10 as build

ENV VER_DOCKER_CE=5:20.10.12~3-0~debian-buster
                  
# add docker repo and install dependencies

RUN apt-get update
RUN apt-get install -y ca-certificates curl gnupg lsb-release

# Add docker official GPG key
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Set up stable repo
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install availible updates;
# Install Docker engine
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y docker-ce=${VER_DOCKER_CE} docker-ce-cli=${VER_DOCKER_CE} containerd.io
# start docker service
RUN service docker start



